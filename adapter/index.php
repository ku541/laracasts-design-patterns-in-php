<?php

require 'vendor/autoload.php';

use Acme\Book;
use Acme\Nook;
use Acme\Kindle;
use Acme\BookInterface;
use Acme\eReaderAdapter;
use Acme\eReaderInterface;

class Person
{
    public function read(BookInterface $book)
    {
        $book->open();
        $book->turnPage();
    }
}

(new Person)->read(new eReaderAdapter(new Nook));