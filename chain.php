<?php

abstract class HomeChecker
{
    protected $successor;

    public abstract function check(HomeStatus $homeStatus);

    public function succeedWith(HomeChecker $successor)
    {
        $this->successor = $successor;
    }

    public function next(HomeStatus $homeStatus)
    {
        if ($this->successor) {
            $this->successor->check($homeStatus);
        }
    }
}

class Locks extends HomeChecker
{
    public function check(HomeStatus $homeStatus)
    {
        if (! $homeStatus->doorsLocked) {
            throw new Exception("The doors are not locked!!!");
        }

        $this->next($homeStatus);
    }
}

class Lights extends HomeChecker
{
    public function check(HomeStatus $homeStatus)
    {
        if (! $homeStatus->lightsOff) {
            throw new Exception("The lights are not off locked!!!");
        }

        $this->next($homeStatus);
    }
}

class Alarm extends HomeChecker
{
    public function check(HomeStatus $homeStatus)
    {
        if (! $homeStatus->alarmOn) {
            throw new Exception("The alarm is not on!!!");
        }

        $this->next($homeStatus);
    }
}

class HomeStatus
{
    public $alarmOn = false;
    public $doorsLocked = true;
    public $lightsOff = true;
}

$locks = new Locks;
$lights = new Lights;
$alarm = new Alarm;

$locks->succeedWith($lights);
$lights->succeedWith($alarm);

$locks->check(new HomeStatus);